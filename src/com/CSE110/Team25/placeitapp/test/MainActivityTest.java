package com.CSE110.Team25.placeitapp.test;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import android.test.ActivityTestCase;
import android.widget.Button;

import com.CSE110.Team25.placeitapp.*;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class MainActivityTest extends ActivityTestCase {
	MainActivity mActivity;
	PlaceIt placeit;
	PlaceitHandler piHandler;

	@Override
	protected void tearDown() throws Exception {
		// TODO Auto-generated method stub
		super.tearDown();
		piHandler.close();
	}

	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		super.setUp();
		mActivity = (MainActivity) getActivity();
		placeit = new PlaceIt("milk", "getmilk", "la", 3.3, 3.0, "la", "la","la", true, "true");
		piHandler = PlaceitHandler.getHelper(mActivity);
	}
	
	public void testAddPlaceIt(){
		
		//When the button to create is clicked
		List<PlaceIt> allPi = piHandler.getAllPlaceIts();
		
		//Then the new placeit will be on the database
		assertEquals(allPi.size(), 0);
	}
	
	
	public void testChangeActiveList(){
		
		//Given the list is in active list
		PlaceIt placeit1 = new PlaceIt("", "milk", "getmilk", 3.3, 3.0, "la", "la","la", false, "");
		PlaceIt placeit2 = new PlaceIt("", "eggs", "geteggs", 4.3, 4.0, "de", "de","de", false, "");
		PlaceIt placeit3 = new PlaceIt("", "apples", "getapples", 5.3, 5.0, "va", "va","va", false, "");
		
		//When the button to go to discard list is clicked
		
		//Then the placeit should show on discard list
		
		//When the button delete is clicked
		
		//Then the list should disappear from the datasource
	}
	
	public void testChangeDiscardList() {
		//Given the list is in in discard list
		
		//When the button active is clicked
		
		//Then the placeit will show on active list and disappear from discard list
		
	}
	
	@Before
	public static void GivenScheduledPlaceit() {
		//Given the scheduled placeit is on active list
		PlaceIt placeitA = new PlaceIt("milk", "getmilk", "la", 3.3, 3.0, "la", "la","la", true, "true");
	}
	@Test
	public void testScheduledThatDay() {
		//When the user goes to the location on non-scheduled day
		piHandler.getAllPlaceItsAround(3.3, 3.0);
		//later get the code for repeat
	@Test
	public void testScheduledDoShow() {
		//Then the notification does not show
		assertNotNull("placeit is shown", mActivity);
	}
	@Test
	public void testScheduledNotThatDay() {
		//When the user goes to the location on scheduled day
		piHandler.getAllPlaceItsAround(50.0, 30.0);
		//later get the code for repeat
		
	}
	@Test
	public void testScheduledDoNotShow() {
		//Then the notification will show
		assertNull("placeit is not shown", mActivity);
	}
	
	public void testMapAddPlaceit() {
		//Given the map
		
		//When the user click on the map
		
		//Then the create placeit will appear
		
	}
	
	@Before
	public void testNotiButton() {
	//Given That the app is running and user looking at placeit notification
	placeit = new PlaceIt("milk", "getmilk", "la", 3.3, 3.0, "la", "la","la", true, "true");
	//showNotification();
	}
	@Test
	public void placeitNotiClickRepost() {
    //When the user presses the re-post button
	Button repost = (Button) findViewById(R.string.Repost);
	repost.performClick();
	}
	@Test
	public void placeitGetRepost() {
	//Then the place-it is reposted with all of the same details as before (re-added to the active list)
		
	}
	@Test
	public void placeitNotiClickDiscard(){
	//When the user clicks on the discard button
	Button discard = (Button) findViewById(R.string.Discard);
	discard.performClick();	
	}
	public void placeitGetDiscard() {
	//Then placeit will show on the discard list
		
	}
	
	public void testNotiMapSinc() {
		//Given the placeit is on the map and notification appear
		
		//When the user delete the placeit on the map
		
		//Then the notification should disappear
		
	}

}
