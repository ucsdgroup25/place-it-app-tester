package com.CSE110.Team25.placeitapp.test;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import android.test.ActivityTestCase;
import android.widget.Button;

import com.CSE110.Team25.placeitapp.*;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import com.CSE110.Team25.placeitapp.MyLocationService;

public class ScenarioTester extends ActivityTestCase {
	MainActivity mActivity;
	PlaceIt placeit;
	PlaceitHandler piHandler;

	@Override
	protected void tearDown() throws Exception {
		// TODO Auto-generated method stub
		super.tearDown();
		piHandler.close();
	}

	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		super.setUp();
		mActivity = (MainActivity) getActivity();
		placeit = new PlaceIt("milk", "getmilk", "la", 3.3, 3.0, "la", "la","la", true, "true");
		piHandler = PlaceitHandler.getHelper(mActivity);
	}
	
	/**
	Title: Creating a non-scheduled Place-It

	As a user
	I want to be able to create a location based reminder (Place-it)
	So that I can remember to do things when at the given location. 

	Scenario 1: User places place-It by specifying location on the map
	*/
	public void testU1S1() {
	/**
		Given the app is open
		When the user clicks the add place-it button
		Then the user is shown the create place-it screen where they are presented with boxes to enter a title, description, and location either by address or selecting on the map
		When the user presses the select location button
		Then the user is shown the map 
		When the user presses a location on the map
		Then they are show a verification message asking if they are sure they would like to place the place-it there
		When the user clicks yes
		Then the place-it is placed
		*/
	}
	/**
	Scenario 2: User places place-it by specifying location by address
		*/
	public void testU1S2() {
	/**
	Given the app is open
	When the user clicks the add place-it button
	Then the user is shown the create place-it screen where they are presented with boxes to enter a title, description, and location either by address or selecting on the map
	When the user specifies a location by typing it into the address box
	And the user presses the see location button
	Then the user is show the map at the specified location
	And the user is shown 
	When the user presses the see location button
	Then they are show a verification message asking if they are sure they would like to place the place-it there
	When the user clicks yes
	Then the place-it is placed
	*/
	}

	/**
	Title: Notification of a place-it
	Estimate: 2 days
	Narrative:

	As a user
	I want to be notified of an active place-it when I approach the location (1/2 mi)
	So that I can be reminded of the thing that I wanted to be reminded of

	Acceptance Criteria: 

	Scenario 1: User is notified when they enter the 1/2 mile radius of an active place-it
		*/
	public void testU2S1() {
	/**
	Given That the app is running
	And there are active place-its
	When The user enters the 1/2 mile radius of the active place-it
	Then The user is notified of their reminder with a top bar notification as well as a sound.
	 */
	}
	
	/**
	Title: Handling a place-it notification (Re-post/discard)
	Estimate: 3 days
	Narrative: 

	As a User
	I want to be able to re-post the place-it when I'm viewing the place-its notification
	So that I do not have to recreate the same place-it if needed.

	Acceptance Criteria: 

	Scenario 1: User chooses to repost place-it
		*/
	
	@Before
	public void testNotiButton() {
	//Given That the app is running and user looking at placeit notification
	placeit = new PlaceIt("milk", "getmilk", "la", 3.3, 3.0, "la", "la","la", true, "true");
	//showNotification();
	}
	@Test
	public void placeitNotiClickRepost() {
    //When the user presses the re-post button
	Button repost = (Button) findViewById(R.string.Repost);
	repost.performClick();
	}
	@Test
	public void placeitGetRepost() {
	//Then the place-it is reposted with all of the same details as before (re-added to the active list)
		
	}
	@Test
	public void placeitNotiClickDiscard(){
	//When the user clicks on the discard button
	Button discard = (Button) findViewById(R.string.Discard);
	discard.performClick();	
	}
	public void placeitGetDiscard() {
	//Then placeit will show on the discard list
		
	}

	/**
	Title: Place-it management
	Estimate: 5 days
	Narrative:

	As a user
	I want to be able to view all of the active place-its in list format
	So that I can easily remove unwanted place-its 

	Acceptance Criteria: 

	Scenario 1: User removes place-it (moves place-it to pull down list)
		*/
	public void testU4S1() {
	/**
	Given that the app is running
	When the user swipes from the left edge of the screen to open the drawer menu
	Then the user is shown a list of active, pulled down, and expired place-its
	When the user selects a place-it from the active list
	Then the user is shown a screen containing the place-it information
	When the user presses the discard button
	Then the place-it is moved to the pulled down list 
	And the user will not receive any more notifications from this place-it
*/
	}
	/**
	Scenario 2: User removes place-it from pull down or expired list
		*/
	public void testU4S2() {
	/**
	Given that the app is running
	When the user swipes from the left edge of the screen to open the drawer menu
	Then the user is shown a list of active, pulled down, and expired place-its
	When the user selects a place-it from the pulled down or expired list
	Then the user is shown a screen containing the place-it information
	When the user presses the discard button
	Then the place-it is deleted
	And the user will not receive any more notifications from this place-it
	And the user will no longer be able to access this place-it
	*/
	}
	/**
	Scenario 3: User reposts place-it from the pulled-down/expired list
	*/
	public void testU4S3() {
	/**
	Given that the app is running
	When the user swipes from the left edge of the screen to open the drawer menu
	Then the user is shown a list of active, pulled down, and expired place-its
	When the user selects a place-it from the pulled down or expired list
	Then the user is shown a screen containing the place-it information
	When the user presses the repost button
	Then the place-it is reposted (moved back to active list)
	And the user will now receive notifications from this place-it
	*/
	}

	/**
	Title: Creating a scheduled place-it
	Estimate: 6 days
	Narrative:

	As a user 
	I want to be able to create a place-it that will notify me on a scheduled basis 
	So that I can remember to do things when at the given location without having to re-create a place-it every time. 

	Acceptance Criteria: 

	Scenario 1: User places a scheduled place-It by specifying location on the map
		*/
	@Before
	public static void GivenScheduledPlaceit() {
		//Given the scheduled placeit is on active list
		PlaceIt placeitA = new PlaceIt("milk", "getmilk", "la", 3.3, 3.0, "la", "la","la", true, "true");
	}
	@Test
	public void testScheduledThatDay() {
		//When the user goes to the location on non-scheduled day
		piHandler.getAllPlaceItsAround(3.3, 3.0);
		//later get the code for repeat
	@Test
	public void testScheduledDoShow() {
		//Then the notification does not show
		assertNotNull("placeit is shown", mActivity);
	}
	@Test
	public void testScheduledNotThatDay() {
		//When the user goes to the location on scheduled day
		piHandler.getAllPlaceItsAround(50.0, 30.0);
		//later get the code for repeat
		
	}
	@Test
	public void testScheduledDoNotShow() {
		//Then the notification will show
		assertNull("placeit is not shown", mActivity);
	}

}
